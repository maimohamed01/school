import { Injectable } from '@angular/core';
import { Item } from '../model/item';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor() { }
  items=[]
  addItems(item:string){
  this.items.push(item)
  }
  itemContainer=[]
  pushItem(item:Item){
  this.itemContainer.push(item)
  }
  titles=[]
  pushTitle(title:string){
    this.titles.push(title)
  }
  descriptions=[]
  pushDescription(desc:string){
    this.descriptions.push(desc)
  }
}
